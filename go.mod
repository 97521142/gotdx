module gitee.com/quant1x/gotdx

go 1.20

require (
	gitee.com/quant1x/gox v1.5.1
	github.com/stretchr/testify v1.8.2
	golang.org/x/exp v0.0.0-20230510235704-dd950f8aeaea
	golang.org/x/text v0.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
